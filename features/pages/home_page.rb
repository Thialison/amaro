class HomePage < SitePrism::Page
    set_url 'https://amaro.com/'

    element :container_home, ".app__home__container"
    element :icon_login, ".i-user"

    def home_page_loaded?
        has_container_home?
    end

    def click_login
        icon_login.click
    end

end