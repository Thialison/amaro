class LoginPage < SitePrism::Page

    element :input_email_cpf, "input[name='emailOrCpf']"
    element :register_now, "a[title='Cadastre-se agora']"
    element :error_email_cpf, ".app__alert is-error"
    element :btn_send, "div.app__form__actions > button[type='submit']"
    element :password, "input[name='password']"
    
    element :form_name, "input[name='name']"
    element :form_last_name, "input[name='lastname']"
    element :form_email, "input[name='email']"
    element :form_cpf, "input[name='cpf']"
    element :form_nasc, "input[name='birthdate']"
    element :form_phone, "input[name='phone']"
    element :form_pass, "input[name='password']"
    element :form_confirm_pass, "input[name='passwordConfirmation']"
    element :login_sucessful , ".app__account__page app__account__dashboard"

    def click_register 
        register_now.click
    end

    def log_with(credencial)
        input_email_cpf.send_keys data(credencial, "email")
        btn_send.click
        password.send_keys data(credencial, "pass")
        btn_send.click
    end

    def fill_valid_information(credencial)
        form_name.send_keys data(credencial, "name")
        form_last_name.send_keys data(credencial, "last_name")
        first("div.form-group > div > input[name='email']").send_keys data(credencial, "email")
        form_cpf.send_keys data(credencial, "cpf")
        form_nasc.send_keys data(credencial, "birth")
        form_phone.send_keys data(credencial, "phone")
        form_pass.send_keys data(credencial, "pass")
        form_confirm_pass.send_keys data(credencial, "confirm_pass")
    end
    
    def fill_invalid_email(credencial)
        input_email_cpf.send_keys data(credencial, "email")
        btn_send.click
    end

    def confirm_registration
        btn_send.click
    end

    def sucessful_displayed?
        has_login_sucessful?
    end
    
    def message_error_displayed?
        has_error_email_cpf?
    end

    def data(credencial, key)
        credencial = credencial.gsub(" ", "_")
        credencial = CREDENCIAIS[credencial.to_sym]
        return credencial = credencial[key.to_sym]
    end

end