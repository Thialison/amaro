Given("I am on amaro's home page") do
    @home = HomePage.new
    @home.load
    @home.home_page_loaded?
end

When("I click on Login icon") do
    @home.click_login
end

When("I login with {string}") do |credencial|
    @login = LoginPage.new
    @login.log_with(credencial)
end

When("I fill the email with {string}") do |credencial|
    @login = LoginPage.new
    @login.fill_invalid_email(credencial)
end

Then("I should be logged on amaro's website") do
    @login.sucessful_displayed?
end

Then("I should see an message error") do
    @login.message_error_displayed?
end

When("I click on register now") do
    @login = LoginPage.new
    @login.click_register
end

When("fill the information wih {string}") do |credencial|
    @login.fill_valid_information(credencial)
end

When("Confirm the register") do
    @login.confirm_registration
end