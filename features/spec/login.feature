Feature: Register User
    As a guest
    I want register on Amaro's website

    Background: Access Login Page
        Given I am on amaro's home page
        When I click on Login icon

    Scenario: Login on Amaro with a valid login
        When I login with "valid login"
        Then I should be logged on amaro's website

    Scenario: Login on Amaro with invalid email
        When I fill the email with "invalid email"
        Then I should see an message error

    Scenario: Register new user with a valid credencials
        When I click on register now
        And fill the information wih "valid credencials"
        And Confirm the register
        Then I should be logged on amaro's website